# Contributor: Alex Yam <alex@alexyam.com>
# Maintainer: Alex Yam <alex@alexyam.com>
pkgname=librime
pkgver=1.8.4
pkgrel=0
pkgdesc="Rime input method engine"
url="https://github.com/rime/librime"
arch="all"
license="BSD-3-Clause"
depends_dev="$pkgname-tools=$pkgver-r$pkgrel"
makedepends="
	boost-dev
	capnproto-dev
	cmake
	glog-dev
	leveldb-dev
	libmarisa-dev
	opencc-dev
	samurai
	yaml-cpp-dev
	"
checkdepends="gtest-dev"
subpackages="$pkgname-dev $pkgname-tools"
source="$pkgname-$pkgver.tar.gz::https://github.com/rime/librime/archive/$pkgver.tar.gz"

build() {
	export CXXFLAGS="$CXXFLAGS -flto=auto"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBOOST_USE_CXX11=ON \
		-DBUILD_DATA=ON \
		-DBUILD_MERGED_PLUGINS=OFF \
		-DBUILD_TEST=$(want_check && echo ON || echo OFF) \
		-DENABLE_EXTERNAL_PLUGINS=ON
	cmake --build build
}

check() {
	cd build && ctest --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	cd "$pkgdir"
	mv -v usr/share/cmake usr/lib/cmake
	# create missing default paths for other ime packages to find rime data
	# (e.g. ibus-rime uses -DRIME_DATA_DIR="/usr/share/rime-data")
	mkdir -v usr/share/rime-data usr/lib/rime-plugins
}

tools() {
	pkgdesc="$pkgdesc (tools)"
	amove usr/bin
}

sha512sums="
422afae2c4af1cc0cbd6404ff1d348d25ca4e30c8180205bf3ec645e5f2e52764a197e59b1faa9d2a28ef5b51eb54d67fc7da9b39eff12735541d86de13c2782  librime-1.8.4.tar.gz
"
