# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kconfigwidgets
pkgver=5.102.0
pkgrel=0
pkgdesc="Widgets for KConfig"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
depends_dev="
	kauth-dev
	kcodecs-dev
	kconfig-dev
	kcoreaddons-dev
	kguiaddons-dev
	ki18n-dev
	kwidgetsaddons-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	kdoctools-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kconfigwidgets-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kstandardactiontest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
b23a340f65843f0bcb3b3ba2aa78cc699ff331aef95723cb994e65637481c9376c9ef66a237400b27c39122f5c410b1659803d1cd4daeb8747e84efd2462ba15  kconfigwidgets-5.102.0.tar.xz
"
