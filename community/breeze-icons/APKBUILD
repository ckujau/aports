# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=breeze-icons
pkgver=5.102.0
pkgrel=0
pkgdesc="Breeze icon themes"
arch="noarch !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-3.0-or-later"
makedepends="
	extra-cmake-modules
	py3-lxml
	python3
	qt5-qtbase-dev
	samurai
	"
checkdepends="bash"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/breeze-icons-$pkgver.tar.xz"

# Several KDE applications use icons not yet present in most themes
# We want to keep the possibility for users to not use the KDE provided
# breeze-icons theme however, as hopefully in the future this situation changes
# Thus let any theme that provides these icons provide "kde-icons" so the user
# retains their ability to choose their preferred theme
provides="kde-icons"
provider_priority=100

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBINARY_ICONS_RESOURCE=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(dupe|symlink)'
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
4660582257a542c2bc215fc3c6281fedfc1c8c3249548358e5eb518c05a0c5f8811e05561c971a720459c1e55b9196a7a130934009069e5b0e809d749c2bc11f  breeze-icons-5.102.0.tar.xz
"
