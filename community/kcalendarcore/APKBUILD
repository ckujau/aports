# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kcalendarcore
pkgver=5.102.0
pkgrel=0
pkgdesc="The KDE calendar access library"
options="!check" # RecursOn-ConnectDaily(2|3|6) make the builders stuck
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later"
depends_dev="qt5-qtbase-dev libical-dev"
makedepends="$depends_dev extra-cmake-modules doxygen qt5-qttools-dev samurai"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcalendarcore-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

replaces="kcalcore"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# testrecurtodo, testreadrecurrenceid, testicaltimezones, testmemorycalendar and testtimesininterval are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "test(recurtodo|readrecurrenceid|icaltimezones|memorycalendar|timesininterval)"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
0af0380f91abd7bc461026ecb15631a3f251441d86dedae7eb5ca6de660bcfa45216d30a3dd583dd41399b1622db5bf7ca034d377209febf188d7331a4e25af7  kcalendarcore-5.102.0.tar.xz
"
